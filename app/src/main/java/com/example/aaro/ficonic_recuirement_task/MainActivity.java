package com.example.aaro.ficonic_recuirement_task;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends AppCompatActivity implements SensorEventListener{

    private TextView xAccText, yAccText, zAccText, lonlatText, speedText, altitudeText, bearingText, accuracyText, timeText;
    private Button btn_start, btn_stop;
    private SensorManager sensorManager;
    private LocationManager locationManager;
    private Sensor mAccelerometer;
    private boolean recording_on;

    String state;
    File Root = Environment.getExternalStorageDirectory();
    File Dir = new File(Root.getAbsolutePath() + "/DataApp");
    File file = new File(Dir, "data.csv");

    static final int REQUEST_LOCATION = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        xAccText = (TextView) findViewById(R.id.xAccText);  // TextViews
        yAccText = (TextView) findViewById(R.id.yAccText);
        zAccText = (TextView) findViewById(R.id.zAccText);
        lonlatText = (TextView) findViewById(R.id.lonlatText);
        speedText = (TextView) findViewById(R.id.speedText);
        altitudeText = (TextView) findViewById(R.id.altitudeText);
        bearingText = (TextView) findViewById(R.id.bearingText);
        accuracyText = (TextView) findViewById(R.id.accuracyText);
        timeText = (TextView) findViewById(R.id.timeText);

        btn_start = (Button) findViewById(R.id.bStart);  // Start button
        btn_start.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     recording_on = true;
                     Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
                 }});
        btn_stop = (Button) findViewById(R.id.bStop);   //Stop Button
        btn_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recording_on = false;
                Toast.makeText(getApplicationContext(), "Recording stopped", Toast.LENGTH_LONG).show();
            }
        });

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE); // Create Sensor Manager
        mAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER); // Accelerometer Sensor
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE); // Create Location Manager

        checkPermissionsLocation();

        getTime();
    }

    public void checkPermissionsLocation(){
        if (ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && (ActivityCompat.checkSelfPermission
                (this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED))
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                                        REQUEST_LOCATION);
        }
        else{
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_LOCATION);
        }
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            int speed = (int) ((location.getSpeed()*3600)/1000);
            float bearing = location.getBearing();
            double altitude = location.getAltitude();
            float accuracy = location.getAccuracy();

            if (recording_on) {
                lonlatText.setText("Latitude: " + latitude + "," + "Longtitude: " + longitude);
                bearingText.setText("Bearing: " + bearing);
                speedText.setText("Speed: " + speed + " km/h");
                altitudeText.setText("Altitude: " + altitude);
                accuracyText.setText("Accuracy: " + accuracy);
                getTime();
                writeDataToCSV();
            }
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
        @Override
        public void onProviderEnabled(String provider) {
        }
        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), "Provider disabled", Toast.LENGTH_LONG).show();
        }
    };

    void getTime(){ // Method for generating time
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        String timestamp = simpleDateFormat.format(new Date());
        timeText.setText(timestamp);
    }

    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (recording_on) {
            xAccText.setText("X: " + event.values[0]);
            yAccText.setText("Y: " + event.values[1]);
            zAccText.setText("Z: " + event.values[2]);
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantedResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantedResults);
        switch (requestCode) {
            case REQUEST_LOCATION:
                break;
        }
    }

    public void writeDataToCSV(){
        state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){
            if(!Dir.exists()){
                Dir.mkdir();
            }
                String entry =  timeText.getText().toString() + "," +
                                lonlatText.getText().toString() + "," +
                                bearingText.getText().toString() + "," +
                                speedText.getText().toString() + "," +
                                altitudeText.getText().toString() + "," +
                                accuracyText.getText().toString() + "," +
                                xAccText.getText().toString() + "," +
                                yAccText.getText().toString() + "," +
                                zAccText.getText().toString() + "," + "\n";
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file, true);
                    fileOutputStream.write(entry.getBytes());
                    fileOutputStream.close();
                }
                catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"File not found", Toast.LENGTH_LONG).show();
                }
                catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"IOException", Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(getApplicationContext(),"SD-card not found", Toast.LENGTH_LONG).show();
            }
    }
}
